import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class ChatService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public chatroomsGet(external_paging): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/chatrooms`,
			params: {external_paging}
		})
	}

	public chatroomsSave(chatroom): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/chatrooms`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: chatroom,
		});
	}

	public chatroomsUpdate(chatroom): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/chatrooms/${chatroom.id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: chatroom,
		});
	}

	public chatroomsDelete(chatroom_id): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/chatrooms/${chatroom_id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public chatGet(chatroom_id, external_paging): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/chat/chatroom/${chatroom_id}`,
			params: {external_paging}
		})
	}

	public chatSave(chat): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/chat`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: chat,
		});
	}

}
