import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ConfigService} from "../config.service";
import {ChatService} from "../chat.service";
import {MatDialog} from "@angular/material/dialog";
import {AuthPanelComponent} from "../auth-panel/auth-panel.component";
import {concatMap, throttleTime} from "rxjs/operators";
import {BehaviorSubject, EMPTY} from "rxjs";
import {get} from 'lodash-es';


@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

	public chatForm: FormGroup = new FormGroup({
		message: new FormControl('')
	})

	public chatroom: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public isLoggedIn: boolean = false;
	public messages: any = [];
	public currentPage: number = 1;
	public hasNextPage: boolean;
	public typingUser: string = null;
	public notProgrammaticScroll = true
	@ViewChild('messageContainer') messageContainer: ElementRef;

	constructor(
		private configService: ConfigService,
		private chatService: ChatService,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.authToken.asObservable().subscribe(res => {
			this.isLoggedIn = !!res;
		});

		this.configService.chatroom.asObservable().pipe(concatMap((chatroom) => {
			this.chatroom.next(chatroom);
			if (this.chatroom.value) {
				return this.chatService.chatGet(this.chatroom.value.id, null)
			} else {
				return EMPTY;
			}
		})).subscribe(chat => {
			this.currentPage = chat.curPage;
			this.hasNextPage = !!chat.nextPage;
			this.messages = chat.items;
			this.scrollToBottom();
		});

		this.chatroom.asObservable().subscribe(res => {
			if (res && get(res, 'id', false)) {
				this.chatForm.reset();
				const channel = this.configService.ably.channels.get(res.id);
				channel.subscribe((message) => {
					if (message?.name === 'typing') {
						if (this.configService.user.value?.id !== message.data?.user.id) {
							this.typingUser = message.data.user?.name;
							setTimeout(() => {
								this.typingUser = null;
							}, 2000)
						}
					} else if (!this.messages.length  || (this.messages[this.messages.length - 1].created_at !== message.timestamp)) {
						this.typingUser = null;
						this.messages.push({created_at: message.timestamp, ...message.data})
						this.scrollToBottom()
					}
				});

				this.chatForm.controls.message.valueChanges.pipe(
					throttleTime(1000)
				).subscribe(res => {
					if (res) {
						channel.publish({name: 'typing', data: {user: this.configService.user.value}});
					}
				})
			}
		});
	}

	public submitMessage(): void {
		if (this.chatForm.controls.message.value) {
			this.chatService.chatSave({
				chatroom_id: this.chatroom.value?.id,
				message: this.chatForm.controls.message.value
			}).pipe(throttleTime(20)).subscribe(res => {
				res.user = {
					id: this.configService.user.value.id,
					name: this.configService.user.value.name
				}
				this.chatForm.reset()
			})
		}
	}

	public login() {
		this.dialog.open(AuthPanelComponent);
	}

	public isFromSelf(user_id): boolean {
		return this.configService.user.value?.id && this.configService.user.value?.id === user_id;
	}

	public scrollToBottom() {
		this.notProgrammaticScroll = false;
		try {
			setTimeout(() => {
				this.messageContainer.nativeElement.scrollTop = this.messageContainer.nativeElement.scrollHeight;
			}, 500);
		} catch (err) {
		}

		setTimeout(() => {
			this.notProgrammaticScroll = true;
		}, 500)
	}

	public loadMore() {
		if (this.hasNextPage && this.notProgrammaticScroll) {
			this.chatService.chatGet(this.chatroom.value?.id, {page: this.currentPage + 1}).subscribe(res => {
				if (res.itemsReceived > 0) {
					this.currentPage = res.curPage;
					this.hasNextPage = !!res.nextPage
					this.messages = [...res.items, ...this.messages];
				}
			});
		}
	}
}
