import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../config.service";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {MatDialog} from "@angular/material/dialog";
import {MobilePanelComponent} from "../mobile-panel/mobile-panel.component";
import { ActivatedRoute } from '@angular/router'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public config: XanoConfig;
	public configured: boolean = false;
	public allowDetail: boolean = false;
	public isSmallScreen: boolean = false;
	public isMediumScreen: boolean = false;
	public showRooms: boolean = true;
	public showDetail: boolean = true;

	constructor(
		private configService: ConfigService,
		private breakpointObserver: BreakpointObserver,
		private dialog: MatDialog,
		private route: ActivatedRoute
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.route.queryParams.subscribe(res => this.configService.xanoApiUrl.next(res?.api_url));
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);

		this.configService.chatroom.asObservable().subscribe(res => {
			this.allowDetail = !!res;
		});

		this.breakpointObserver.observe([Breakpoints.Medium]).subscribe(result => {
			this.isMediumScreen = result.matches;
			this.showDetail = !result.matches
		});

		this.breakpointObserver.observe([Breakpoints.Small, Breakpoints.XSmall]).subscribe(result => {
			this.isSmallScreen = result.matches;
			this.showDetail = !result.matches;
			this.showRooms = !result.matches
		});
	}

	public toggle(panel): void {
		if (panel === 'rooms') {
			if (!this.isSmallScreen) {
				this.showRooms = !this.showRooms
			} else {
				this.dialog.open(MobilePanelComponent, {data: {rooms: true}});
			}
		} else if (panel === 'detail') {
			if (!this.isMediumScreen && !this.isSmallScreen) {
				this.showDetail = !this.showDetail
			} else {
				this.dialog.open(MobilePanelComponent, {data: {detail: true}});
			}
		}
	}

}
