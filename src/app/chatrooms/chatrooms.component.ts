import {Component, OnInit} from '@angular/core';
import {ChatService} from "../chat.service";
import {ConfigService} from "../config.service";


export interface Chatrooms {
	curPage: number,
	items: any[],
	itemsReceived: number,
	nextPage: number,
	prevPage: number,
}

@Component({
	selector: 'app-chatrooms',
	templateUrl: './chatrooms.component.html',
	styleUrls: ['./chatrooms.component.scss']
})
export class ChatroomsComponent implements OnInit {
	public chatrooms = [];
	public currentPage: number = 1;
	public hasNextPage: boolean;
	public activeRoom: any;
	public noRecordsFound: boolean = false;

	constructor(private chatService: ChatService, private configService: ConfigService) {
	}

	ngOnInit(): void {
		this.chatService.chatroomsGet(null).subscribe(res => {
			this.chatrooms = res.items;
			this.noRecordsFound = !!res;
			this.hasNextPage = !!res.nextPage
			this.currentPage = res.currentPage
		})

		this.configService.newChatroom.asObservable().subscribe(res => {
			if (res) {
				this.chatrooms.unshift(res)
				this.configService.newChatroom.next(null);
			}
		});

		this.configService.deletedChatroom.asObservable().subscribe(res => {
			if (res) {
				this.chatrooms = this.chatrooms.filter(x => x.id !== res)
			}
		});
	}

	public enterChatroom(room) {
		this.activeRoom = room;
		this.configService.chatroom.next(room);
	}

	public loadMore() {
		if (this.hasNextPage) {
			this.chatService.chatroomsGet({page: this.currentPage + 1}).subscribe(res => {
				if (res.itemsReceived > 0) {
					this.currentPage = res.curPage;
					this.hasNextPage = !!res.nextPage
					this.chatrooms = [...this.chatrooms, ...res.items];
				}
			});
		}
	}

}
