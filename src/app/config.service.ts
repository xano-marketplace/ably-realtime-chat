import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import * as Ably from 'ably';
import {Types} from 'ably';
import {v4 as uuid} from 'uuid';
import ClientOptions = Types.ClientOptions;

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null)
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public chatroom: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public newChatroom: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public deletedChatroom: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public ablyKey: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public clientID = uuid();

	public ably;

	public config: XanoConfig = {
		title: 'Xano Ably Realtime Chat',
		summary: 'This is full functioning public chat utilizing ably realtime.',
		editText: 'Get source code',
		editLink: '',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
                	This demo is provides a full functioning implementation of ably chat utilizing Xano as 
                	your backend tool for saving chat history as well as for authentication.
                </p>              
                <b>
                	Before using this demo you will need to have an ably api key and add it in the config panel.
                </b>
                <br />     <br />
                <p>This demo consists of following components:</p>
                <h2>Personal View</h2>
                <p>This is where you can join chat-rooms and chat.</p>
                <h2>Manage View</h2>
                <p>This is a side panel that lets you add a new chat-rooms or edit an existing one.</p>
                <h2>Search</h2>
                <p>A search box is present to allow you to search chat-rooms. There is also support for filtering based on completed and important attributes.</p>
                <p> You can also search a chat-room for a message you are looking for.</p>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/chat/chatroom/{chatroom_id}',
			'/chatrooms',
			'/chatrooms/{chatrooms_id}'
		]
	};

	constructor(private apiService: ApiService, private xanoService: XanoService) {
		this.ablyKey.asObservable().subscribe(res => {
			if (res) {
				let clientOptions: ClientOptions = {
					key: this.ablyKey.value,
					clientId: this.clientID
				}
				this.ably = new Ably.Realtime(clientOptions);
			}
		})

	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
