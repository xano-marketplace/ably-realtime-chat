import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {AuthPanelComponent} from './auth-panel/auth-panel.component';
import {ManagePanelComponent} from './manage-panel/manage-panel.component';
import {SafeHtmlPipe} from "./safe-html.pipe";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {MatCommonModule} from "@angular/material/core";
import {ChatroomsComponent} from './chatrooms/chatrooms.component';
import {ChatComponent} from './chat/chat.component';
import {ChatroomDetailComponent} from './chatroom-detail/chatroom-detail.component';
import {MatDividerModule} from "@angular/material/divider";
import {MatListModule} from "@angular/material/list";
import {MatMenuModule} from "@angular/material/menu";
import {MobilePanelComponent} from './mobile-panel/mobile-panel.component';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {InfiniteScrollModule} from "ngx-infinite-scroll";

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		ActionBarComponent,
		ConfigPanelComponent,
		AuthPanelComponent,
		ManagePanelComponent,
		SafeHtmlPipe,
		ChatroomsComponent,
		ChatComponent,
		ChatroomDetailComponent,
		MobilePanelComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatCommonModule,
		HttpClientModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		MatSnackBarModule,
		MatCardModule,
		MatInputModule,
		MatDialogModule,
		MatDividerModule,
		MatListModule,
		MatMenuModule,
		ScrollingModule,
		InfiniteScrollModule
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		}
	}],
	bootstrap: [AppComponent]
})
export class AppModule {
}
