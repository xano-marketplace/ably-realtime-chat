import {Component, Inject, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../config.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ChatService} from "../chat.service";
import {finalize, tap} from "rxjs/operators";
import {get} from 'lodash-es';

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {
	public config: XanoConfig;
	public deleting: boolean = false;
	public saving: boolean = false;

	public chatForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		title: new FormControl('', Validators.required),
		description: new FormControl(''),
		topic: new FormControl('')
	});


	constructor(
		private configService: ConfigService,
		private dialogRef: MatDialogRef<ManagePanelComponent>,
		private chatService: ChatService,
		@Inject(MAT_DIALOG_DATA) public data,
		private snackBar: MatSnackBar) {
	}


	ngOnInit(): void {
		this.config = this.configService.config;
		this.chatForm.patchValue({user_id: this.configService.user?.value?.id});

		if (this.data?.chat) {
			this.chatForm.patchValue(this.data.chat);

		}
	}

	public submit(): void {
		this.chatForm.markAllAsTouched();
		if (this.chatForm.valid) {
			if (!this.data?.chat) {
				this.chatService.chatroomsSave({
					...this.chatForm.value
				}).pipe(
					tap(x => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({new: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			} else {
				this.chatService.chatroomsUpdate({
					...this.chatForm.value,
				}).pipe(
					tap(() => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({updated: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			}
		} else {
			if (!this.chatForm.controls.user_id.value) {
				this.snackBar.open('Please Login', 'Error', {panelClass: 'error-snack'});
			} else {
				this.snackBar.open('Please Fix Form', 'Error', {panelClass: 'error-snack'});
			}
		}
	}

	delete() {
		this.chatService.chatroomsDelete(this.data.chat.id)
			.pipe(
				tap(() => this.deleting = true),
				finalize(() => this.deleting = false)
			)
			.subscribe((res: any) => {
				this.dialogRef.close({deleted: true, item: {id: this.data.chat.id}});
			}, err => {
				this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
			});
	}
}
