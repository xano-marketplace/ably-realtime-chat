import {Component, OnInit} from '@angular/core';
import {ChatService} from "../chat.service";
import {ConfigService} from "../config.service";
import {MatDialog} from "@angular/material/dialog";
import {ManagePanelComponent} from "../manage-panel/manage-panel.component";
import {get} from 'lodash-es';


@Component({
	selector: 'app-chatroom-detail',
	templateUrl: './chatroom-detail.component.html',
	styleUrls: ['./chatroom-detail.component.scss']
})
export class ChatroomDetailComponent implements OnInit {
	public chatroom: any;
	public memberCount: number = 0;
	private channel: any;

	constructor(
		private chatService: ChatService,
		private configService: ConfigService,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.chatroom.asObservable().subscribe(res => {
			if(res && get(res, 'id', false)) {
				this.chatroom = res;

				if (this.channel) {
					this.channel.presence.leave('left')
				}
				this.channel = this.configService.ably.channels.get(res.id);
				this.channel.presence.enter('entered');

				this.channel.presence.subscribe(res => {
					this.memberCount = this.channel.presence.members.list().length
				})
			}
		});
	}


	public editRoom() {
		const dialogRef = this.dialog.open(ManagePanelComponent, {data: {chat: this.chatroom}});

		dialogRef.afterClosed().subscribe(res => {
			if (res?.updated) {
				this.chatroom = res.item;
				this.configService.chatroom.next(res.item);
			} else if (res?.deleted) {
				this.chatroom = undefined;
				this.configService.deletedChatroom.next(res.item.id);
				this.configService.chatroom.next(null);
			}
		})
	}

	public allowEdit(): boolean {
		return this.chatroom.user_id && this.configService.user.value?.id &&
			(this.chatroom.user_id === this.configService.user.value.id);
	}

}
