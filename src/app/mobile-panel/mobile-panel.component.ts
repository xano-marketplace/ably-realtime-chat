import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
	selector: 'app-mobile-panel',
	templateUrl: './mobile-panel.component.html',
	styleUrls: ['./mobile-panel.component.scss']
})
export class MobilePanelComponent implements OnInit {

	constructor(
		private dialogRef: MatDialogRef<MobilePanelComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
	) {
	}

	ngOnInit(): void {
	}

}
